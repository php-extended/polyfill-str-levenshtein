# php-extended/polyfill-str-levenshtein

A php implementation of the levenshtein distance without length limitations.


![coverage](https://gitlab.com/php-extended/polyfill-str-levenshtein/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/polyfill-str-levenshtein/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/polyfill-str-levenshtein": "^1"`


## Basic Usage

This library gives the function `str_levenshtein(?string $str1, ?string $str2) : int` :

```php

$distance = \str_levenshtein('toto', 'tata');
// $distance is 2

```


## License

MIT (See [license file](LICENSE)).
