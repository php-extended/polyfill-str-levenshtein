<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-str-levenshtein library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Polyfill;

/**
 * StrLevenshtein class file.
 * 
 * This class encapsulate the unlimited levenshtein function.
 * 
 * @author Anastaszor
 */
final class StrLevenshtein
{
	
	/**
	 * Returns the levenshtein distance between the two strings. A null or
	 * an empty string is treated as the same as a string of length zero.
	 * 
	 * @param ?string $str1
	 * @param ?string $str2
	 * @return integer
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public static function strLevenshtein(?string $str1, ?string $str2) : int
	{
		if($str1 === $str2)
		{
			return 0;
		}
		
		if(null === $str1 || '' === $str1 || null === $str2 || '' === $str2)
		{
			return \abs(\strlen((string) $str1) - \strlen((string) $str2));
		}
		
		$len2 = (int) \mb_strlen($str2, '8bit');
		$len1 = (int) \mb_strlen($str1, '8bit');
		
		if(256 > $len1 && 256 > $len2)
		{
			$native = \levenshtein($str1, $str2);
			if(0 < $native)
			{
				return $native;
			}
		}
		
		/** @var array<integer, integer> $dis */
		$dis = \range(0, $len2);
		$disNew = [];
		
		for($x = 1; $x <= $len1; $x++)
		{
			$disNew[0] = $x;
			
			for($y = 1; $y <= $len2; $y++)
			{
				$count = ($str1[$x - 1] === $str2[$y - 1]) ? 0 : 1;
				$disNew[$y] = \min((int) $dis[$y] + 1, (int) $disNew[$y - 1] + 1, (int) $dis[$y - 1] + $count);
			}
			
			$dis = $disNew;
		}
		
		return (int) $dis[$len2];
	}
	
}
