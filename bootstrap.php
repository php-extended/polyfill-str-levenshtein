<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-str-levenshtein library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Polyfill\StrLevenshtein;

if(!\function_exists('str_levenshtein'))
{
	/**
	 * Calculate Levenshtein distance between two strings.
	 * 
	 * @link http://www.php.net/manual/en/function.levenshtein.php
	 * @param ?string $str1 One of the strings being evaluated for Levenshtein distance.
	 * @param ?string $str2 One of the strings being evaluated for Levenshtein distance.
	 * @return integer This function returns the Levenshtein-Distance between the two argument strings
	 */
	function str_levenshtein(?string $str1, ?string $str2) : int
	{
		return StrLevenshtein::strLevenshtein($str1, $str2);
	}
}
