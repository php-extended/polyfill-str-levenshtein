<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-str-levenshtein library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Polyfill\StrLevenshtein;
use PHPUnit\Framework\TestCase;

/**
 * StrLevenshteinTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Polyfill\StrLevenshtein
 *
 * @internal
 *
 * @small
 */
class StrLevenshteinTest extends TestCase
{	
	/**
	 * The object to test.
	 * 
	 * @var StrLevenshtein
	 */
	protected $_object;
	
	public function testLevenshteinIdentic() : void
	{
		$this->assertEquals(0, $this->_object->strLevenshtein('toto', 'toto'));
	}
	
	public function testLevenshteinNulls() : void
	{
		$this->assertEquals(0, $this->_object->strLevenshtein(null, null));
	}
	
	public function testLevenshteinEmptys() : void
	{
		$this->assertEquals(0, $this->_object->strLevenshtein('', null));
	}
	
	public function testLevenshteinLeftNull() : void
	{
		$this->assertEquals(4, $this->_object->strLevenshtein(null, 'toto'));
	}
	
	public function testLevenshteinRightNull() : void
	{
		$this->assertEquals(4, $this->_object->strLevenshtein('toto', null));
	}
	
	public function testLevenshteinSmall() : void
	{
		$this->assertEquals(2, $this->_object->strLevenshtein('toto', 'tata'));
	}
	
	public function testLevenshteinBigAdd() : void
	{
		$this->assertEquals(1, $this->_object->strLevenshtein(
			'azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890',
			'azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn123445678901',
		));
	}
	
	public function testLevenshteinBigSubs() : void
	{
		$this->assertEquals(1, $this->_object->strLevenshtein(
			'azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890',
			'azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn1234456789',
		));
	}
	
	public function testLevenshteinBigChange() : void
	{
		$this->assertEquals(1, $this->_object->strLevenshtein(
			'azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890',
			'azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn12344567890azertyuiopqsdfghjklmwxcvbn1234456789A',
		));
	}
	
	public function nativeStr() : void
	{
		$this->assertEquals(0, \str_levenshtein('toto', 'toto'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StrLevenshtein();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
